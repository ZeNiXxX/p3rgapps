#
# Copyright (C) 2018 The Google Pixel3ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#vendor/p3r/p3rgapps/app/

# Static proprietary libraries (32-bit)
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/lib/libaudio-resampler.so:system/lib/libaudio-resampler.so \
        vendor/p3r/p3rgapps/lib/libchrome.so:system/lib/libchrome.so \
        vendor/p3r/p3rgapps/lib/libdl.so:system/lib/libdl.so \
        vendor/p3r/p3rgapps/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so \
        vendor/p3r/p3rgapps/lib/libfilterpack_imageproc.so:system/lib/libfilterpack_imageproc.so \
        vendor/p3r/p3rgapps/lib/libfrsdk.so:system/lib/libfrsdk.so \
        vendor/p3r/p3rgapps/lib/librcc.so:system/lib/librcc.so

# Static proprietary libraries (64-bit)
PRODUCT_COPY_FILES += \
        vendor/p3r/p3rgapps/lib64/libaptXHD_encoder.so:system/lib64/libaptXHD_encoder.so \
        vendor/p3r/p3rgapps/lib64/libaptX_encoder.so:system/lib64/libaptX_encoder.so \
        vendor/p3r/p3rgapps/lib64/libaudio-resampler.so:system/lib64/libaudio-resampler \
        vendor/p3r/p3rgapps/lib64/libbarhopper.so:system/lib64/libbarhopper.so \
        vendor/p3r/p3rgapps/lib64/libbinderwrapper.so:system/lib64/libbinderwrapper.so \
        vendor/p3r/p3rgapps/lib64/libbrillo-binder.so:system/lib64/libbrillo-binder.so \
        vendor/p3r/p3rgapps/lib64/libbrillo-stream.so:system/lib64/libbrillo-stream.so \
        vendor/p3r/p3rgapps/lib64/libbrillo.so:system/lib64/libbrillo.so \
        vendor/p3r/p3rgapps/lib64/libchrome.so:system/lib64/libchrome.so \
        vendor/p3r/p3rgapps/lib64/libdl.so:system/lib64/libdl.so \
        vendor/p3r/p3rgapps/lib64/libfacenet.so:system/lib64/libfacenet.so \
        vendor/p3r/p3rgapps/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so \
        vendor/p3r/p3rgapps/lib64/libfilterpack_imageproc.so:system/lib64/libfilterpack_imageproc.so \
        vendor/p3r/p3rgapps/lib64/libfrsdk.so:system/lib64/libfrsdk.so \
        vendor/p3r/p3rgapps/lib64/librcc.so:system/lib64/librcc.so \
        vendor/p3r/p3rgapps/lib64/libsketchology_native.so:system/lib64/libsketchology_native.so

# Google prebuilt apps
PRODUCT_PACKAGES += \
	Chrome \
	ConferenceDialer \
	FaceLock \
	GoogleDialer \
	GoogleContactsSyncAdapter \
	GoogleExtShared \
	GooglePrintRecommendationService \
	GoogleTTS \
	MarkupGoogle \
	NexusWallpapersStubPrebuilt2018 \
	Ornament \
	PlayAutoInstallConfig \
	PrebuiltDeskClockGoogle \
	PrebuiltGmail \
	SMSConnectPrebuilt \
	SoundPickerPrebuilt \
	Tycho \
	WallpapersBReel2018 \
	WebViewStub \
	talkback \
	AndroidMigratePrebuilt \
	AndroidPlatformServices \
	AppDirectedSMSService \
	CarrierServices \
	CarrierSettings \
	CarrierSetup \
	ConnMetrics \
	DocumentsUI \
	GCS \
	GoogleBackupTransport \
	GoogleExtServices \
	GoogleFeedback \
	GoogleOneTimeInitializer \
	GooglePartnerSetup \
	GoogleServicesFramework \
	MatchmakerPrebuilt \
	Phonesky \
	PrebuiltGmsCorePi \
	SCONE \
	SettingsIntelligenceGooglePrebuilt \
	SetupWizard \
	StorageManagerGoogle \
	TagGoogle \
	TimeZoneDataPrebuilt \
	TimeZoneUpdater \
	Velvet \
	WallpaperPickerGooglePrebuilt \
	WellbeingPrebuilt


# Chimera modules
PRODUCT_PACKAGES += \
	AdsDynamite \
	CronetDynamite \
	DynamiteLoader \
	DynamiteModulesA \
	DynamiteModulesC \
	GoogleCertificates \
	MapsDynamite

# Google Camera
ifeq ($(TARGET_USES_GOOGLE_CAMERA),true)
PRODUCT_PACKAGES += \
	GoogleCamera \
	com.google.android.camera.experimental2016

PRODUCT_COPY_FILES += \
        vendor/p3r/p3rgapps/etc/permissions/com.google.android.camera.experimental2016.xml:system/etc/permissions/com.google.android.camera.experimental2016.xml
else
PRODUCT_PACKAGES += \
	GoogleCamera2
endif


# Shared Google proprietary libraries
PRODUCT_PACKAGES += \
	com.google.android.dialer.support \
	com.google.android.maps \
	com.google.android.media.effects

# Various proprietary files
# System configurations
PRODUCT_COPY_FILES += \
        vendor/p3r/p3rgapps/etc/sysconfig/google-hiddenapi-package-whitelist.xml:system/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/google_vr_build.xml:system/etc/sysconfig/google_vr_build.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/nexus.xml:system/etc/sysconfig/nexus.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/pixel_2018_exclusive.xml:system/etc/sysconfig/pixel_2017_exclusive.xml \
        vendor/p3r/p3rgapps/etc/sysconfig/pixel_experience_2017.xml:system/etc/sysconfig/pixel_experience_2017.xml \
	vendor/p3r/p3rgapps/etc/sysconfig/pixel_experience_2018.xml:system/etc/sysconfig/pixel_experience_2018.xml

# Permissions
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml \
	vendor/p3r/p3rgapps/etc/permissions/com.google.android.hardwareinfo.xml:system/etc/permissions/com.google.android.hardwareinfo.xml \
	vendor/p3r/p3rgapps/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	vendor/p3r/p3rgapps/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml \
	vendor/p3r/p3rgapps/etc/permissions/com.google.vr.platform.xml:system/etc/permissions/com.google.vr.platform.xml \
	vendor/p3r/p3rgapps/etc/permissions/privapp-permissions-google.xml:system/etc/permissions/privapp-permissions-google.xml

# Preferred Apps
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml

# SCONE
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/etc/scone/country_border.leveldb:system/etc/scone/country_border.leveldb

# Overlay
DEVICE_PACKAGE_OVERLAYS += \
	vendor/p3r/p3rgapps/overlay

# shared ime files
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/usr/share/ime/google/d3_lms/ko_2018030706.zip:system/usr/share/ime/google/d3_lms/ko_2018030706.zip \
	vendor/p3r/p3rgapps/usr/share/ime/google/d3_lms/mozc.data:system/usr/share/ime/google/d3_lms/mozc.data \
	vendor/p3r/p3rgapps/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip:system/usr/share/ime/google/zh_CN_2018030706.zip

# en-US srec
PRODUCT_COPY_FILES += \
	vendor/p3r/p3rgapps/usr/srec/en-US/APP_NAME.fst:system/usr/srec/en-US/APP_NAME.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/APP_NAME.syms:system/usr/srec/en-US/APP_NAME.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/CLG.prewalk.fst:system/usr/srec/en-US/CLG.prewalk.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/CONTACT_NAME.fst:system/usr/srec/en-US/CONTACT_NAME.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/CONTACT_NAME.syms:system/usr/srec/en-US/CONTACT_NAME.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/SONG_NAME.fst:system/usr/srec/en-US/SONG_NAME.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/SONG_NAME.syms:system/usr/srec/en-US/SONG_NAME.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/am_phonemes.syms:system/usr/srec/en-US/am_phonemes.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/app_bias.fst:system/usr/srec/en-US/app_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/c_fst:system/usr/srec/en-US/c_fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/commands.abnf:system/usr/srec/en-US/commands.abnf \
	vendor/p3r/p3rgapps/usr/srec/en-US/compile_grammar.config:system/usr/srec/en-US/compile_grammar.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/config.pumpkin:system/usr/srec/en-US/config.pumpkin \
	vendor/p3r/p3rgapps/usr/srec/en-US/confirmation_bias.fst:system/usr/srec/en-US/confirmation_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/contacts.abnf:system/usr/srec/en-US/contacts.abnf \
	vendor/p3r/p3rgapps/usr/srec/en-US/contacts_bias.fst:system/usr/srec/en-US/contacts_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/contacts_disambig.fst:system/usr/srec/en-US/contacts_disambig.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/dict:system/usr/srec/en-US/dict \
	vendor/p3r/p3rgapps/usr/srec/en-US/dictation.config:system/usr/srec/en-US/dictation.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/dnn:system/usr/srec/en-US/dnn \
	vendor/p3r/p3rgapps/usr/srec/en-US/embedded_class_denorm.mfar:system/usr/srec/en-US/embedded_class_denorm.mfar \
	vendor/p3r/p3rgapps/usr/srec/en-US/embedded_normalizer.mfar:system/usr/srec/en-US/embedded_normalizer.mfar \
	vendor/p3r/p3rgapps/usr/srec/en-US/endpointer_dictation.config:system/usr/srec/en-US/endpointer_dictation.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/endpointer_model:system/usr/srec/en-US/endpointer_model \
	vendor/p3r/p3rgapps/usr/srec/en-US/endpointer_model.mmap:system/usr/srec/en-US/endpointer_model.mmap \
	vendor/p3r/p3rgapps/usr/srec/en-US/endpointer_voicesearch.config:system/usr/srec/en-US/endpointer_voicesearch.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/ep_portable_mean_stddev:system/usr/srec/en-US/ep_portable_mean_stddev \
	vendor/p3r/p3rgapps/usr/srec/en-US/ep_portable_model.uint8.mmap:system/usr/srec/en-US/ep_portable_model.uint8.mmap \
	vendor/p3r/p3rgapps/usr/srec/en-US/g2p.data:system/usr/srec/en-US/g2p.data \
	vendor/p3r/p3rgapps/usr/srec/en-US/g2p_fst:system/usr/srec/en-US/g2p_fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/g2p_graphemes.syms:system/usr/srec/en-US/g2p_graphemes.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/g2p_phonemes.syms:system/usr/srec/en-US/g2p_phonemes.syms \
	vendor/p3r/p3rgapps/usr/srec/en-US/grammar.config:system/usr/srec/en-US/grammar.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/hmm_symbols:system/usr/srec/en-US/hmm_symbols \
	vendor/p3r/p3rgapps/usr/srec/en-US/hmmlist:system/usr/srec/en-US/hmmlist \
	vendor/p3r/p3rgapps/usr/srec/en-US/input_mean_std_dev:system/usr/srec/en-US/input_mean_std_dev \
	vendor/p3r/p3rgapps/usr/srec/en-US/lexicon.U.fst:system/usr/srec/en-US/lexicon.U.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/lstm_model.uint8.data:system/usr/srec/en-US/lstm_model.uint8.data \
	vendor/p3r/p3rgapps/usr/srec/en-US/magic_mic.config:system/usr/srec/en-US/magic_mic.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/media_bias.fst:system/usr/srec/en-US/media_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/metadata:system/usr/srec/en-US/metadata \
	vendor/p3r/p3rgapps/usr/srec/en-US/monastery_config.pumpkin:system/usr/srec/en-US/monastery_config.pumpkin \
	vendor/p3r/p3rgapps/usr/srec/en-US/norm_fst:system/usr/srec/en-US/norm_fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/offensive_word_normalizer.mfar:system/usr/srec/en-US/offensive_word_normalizer.mfar \
	vendor/p3r/p3rgapps/usr/srec/en-US/offline_action_data.pb:system/usr/srec/en-US/offline_action_data.pb \
	vendor/p3r/p3rgapps/usr/srec/en-US/phonelist:system/usr/srec/en-US/phonelist \
	vendor/p3r/p3rgapps/usr/srec/en-US/portable_lstm:system/usr/srec/en-US/portable_lstm \
	vendor/p3r/p3rgapps/usr/srec/en-US/portable_meanstddev:system/usr/srec/en-US/portable_meanstddev \
	vendor/p3r/p3rgapps/usr/srec/en-US/pumpkin.mmap:system/usr/srec/en-US/pumpkin.mmap \
	vendor/p3r/p3rgapps/usr/srec/en-US/read_items_bias.fst:system/usr/srec/en-US/read_items_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/rescoring.fst.compact:system/usr/srec/en-US/rescoring.fst.compact \
	vendor/p3r/p3rgapps/usr/srec/en-US/semantics.pumpkin:system/usr/srec/en-US/semantics.pumpkin \
	vendor/p3r/p3rgapps/usr/srec/en-US/skip_items_bias.fst:system/usr/srec/en-US/skip_items_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/time_bias.fst:system/usr/srec/en-US/time_bias.fst \
	vendor/p3r/p3rgapps/usr/srec/en-US/transform.mfar:system/usr/srec/en-US/transform.mfar \
	vendor/p3r/p3rgapps/usr/srec/en-US/voice_actions.config:system/usr/srec/en-US/voice_actions.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/voice_actions_compiler.config:system/usr/srec/en-US/voice_actions_compiler.config \
	vendor/p3r/p3rgapps/usr/srec/en-US/word_confidence_classifier:system/usr/srec/en-US/word_confidence_classifier \
	vendor/p3r/p3rgapps/usr/srec/en-US/wordlist.syms:system/usr/srec/en-US/wordlist.syms
